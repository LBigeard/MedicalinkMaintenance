<div align="center">

<img src="logo_medicalink.png" align="center" width="150" alt="Project icon">
<p align="center"><b>This project is licensed under the EUPL 1.2. For more details, see the <a href="LICENSE.md">LICENSE</a> file.</b></p>
<p align="center"><i>"An Application that allows users to scan prescriptions, medications, and sends notifications for intakes"</i></p>
</div>

## Demo

[Youtube](https://youtu.be/w_DdduQS2IA)

## Authors

### Main Squad

* Nicolas Chusseau ([NicolasChusseau](https://github.com/NicolasChusseau))
* Pacôme Cailleteau ([PacomeCailleteau](https://github.com/PacomeCailleteau))
* Lilian Delhommeau (E211556C) ([Redly0n](https://github.com/Redly0n))

### Maintenance Squad

* Pacôme Cailleteau ([PacomeCailleteau](https://github.com/PacomeCailleteau))
* Lucas Bigeard ([LucWaw](https://github.com/LucWaw))
* Swan Gonzales ([SwanGonzales](https://github.com/SwanGon))
* Arthur Osselin ([Arthur Osselin](https://github.com/tuturita))

## Related Git repository

* Main Android
  application : [Medicalink](https://gitlab.univ-nantes.fr/E211556C/sae_medicalink#----)
* Maintenance application : [MedicalinkMaintenance](https://github.com/LucWaw/MedicalinkMaintenance)
* Artificial Intelligence Generator (Named Entity
  Recognition) : [MedicAppAI](https://github.com/anrouxel/MedicAppAI#----)
* Named Entity Recognition AI model for prescriptions (usable as a
  sub-module) : [MedicAppAssets](https://gitlab.univ-nantes.fr/E213726L/MedicAppAssets.git)

## Special thanks

* Antonin Rouxel ([anrouxel](https://github.com/anrouxel)) for AI Generator and Named Entity
  Recognition AI model for prescriptions.